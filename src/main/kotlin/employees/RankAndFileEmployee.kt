package employees

import Employee
import java.time.LocalDate

class RankAndFileEmployee(
        firstName: String,
        lastName: String,
        department: String,
        hiringDate: LocalDate,
        location: LocationDetails
): Employee (
        firstName,
        lastName,
        department,
        hiringDate,
        location
) {
        val isRankedAndFile = false
        val isAdmin = true

        //return the value of isRankAndFile
        override fun doSomething():String = isRankedAndFile.toString()
}