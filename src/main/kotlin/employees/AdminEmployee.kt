import employees.LocationDetails
import java.time.LocalDate

class AdminEmployee(
        firstName: String,
        lastName: String,
        department: String,
        hiringDate: LocalDate,
location:LocationDetails
):Employee(
        firstName,
        lastName,
        department,
        hiringDate,
        location
){
        val isRankedAndFile = false
        val isAdmin = true
        //return the value of isRankAndFile
        override fun doSomething():String = isAdmin.toString()
}