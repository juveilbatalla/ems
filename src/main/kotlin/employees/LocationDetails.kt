package employees

import javax.swing.plaf.synth.Region

/*
* A class containing all the details about the office location of an employee
* City, Province, Region, Exact Address*/

//Data Class -> automatic string conversion (toString())

//equal() checks the actual data
//we can use copy() function , it create a copy of an object

//holding information
data class LocationDetails(var city: String, var province: String, var Region: String, var exactAddress: String)
//Stringified value of the instantiated object (JSON like)
//OUTPUT sa DATA CLASS
//LocationDetails(city=Makati, province=Metro Manila, Region=NCR, exactAddress=1124 XYZ Building, Legaspi Village)

//Map version of the object above
//{city=Makati, province=Metro Manila, Region=NCR, exactAddress=1124 XYZ Building, Legaspi Village}