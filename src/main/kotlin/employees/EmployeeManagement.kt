package employees

import AdminEmployee
import java.time.LocalDate

fun main() {
//    val employee2 = AdminEmployee("Brandon", "Cruz", "HR", LocalDate.now()).apply {
//        this.firstName = firstName.uppercase()
//
//    }
    val address1 = LocationDetails("Makati", "Metro Manila", "NCR", "1124 XYZ Building, Legaspi Village")

    val employee1 = RankAndFileEmployee("Amy", "Parker", "IT", LocalDate.now(), address1)
    println(employee1.location)
    //with apply, let, run

//    with(employee2) {
//        println(firstName)
//        println(lastName)
//        println(employeeID)
//        println(department)
//        println(isRankedAndFile)
//        println(isAdmin)
//    }

//    println(employee2.showEmployeeData())
//    println(employee2.isRankedAndFile)
//    println(employee2.isAdmin)
//    println(employee2.employeeID)
//
//    println(employee2.firstName)
//
//    println(employee2.firstName)
//    val employee4 = AdminEmployee("Amy", "Parker", "IT", LocalDate.now())


    val address2 = address1.copy(
        //to change kahit isa lang na attri in data class
        exactAddress = "1623 HJK Building, Salcedo Building"
    )

//    println(address1 )
//    println(address2 )
//    println(employee2==employee1)
//    println(address2==address1)
}