import employees.EmployeeInterface
import employees.LocationDetails
import java.time.LocalDate
import javax.xml.stream.Location


/**
 * This class should have the following attributes:
 * firstName
 * lastName
 * department
 * hiringDate - (cannot b modifkied)
 */

abstract class Employee(
    override var firstName: String,
    override var lastName: String,
    var department: String,
    val hiringDate: LocalDate,
    var location: LocationDetails
):EmployeeInterface {
    var employeeID: String = " "
    override fun doSomething():String = "Sample String"
    /**
     *   * Create a function that shows the data of this employee
     * "Employee Name: from  department."
     */

    fun showEmployeeData() = println("Employee Name:$firstName $lastName from $department department.")

    /**
     *      * Generate an id, and save as an attribute of this class
     * the id format is: first 2 letters of the firstName + - + first
     * 2 letters of the lastName + - + 8 random digits

     */

    init {
        employeeID = "${firstName.substring(0, 2).uppercase()}-${lastName.substring(0, 2).uppercase()}-" +
                "${(10000000 until 99999999).random()}"
//        println(employeeID)
    }
}
